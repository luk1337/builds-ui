FROM python:3.6

ARG VERSION=dev

ENV VERSION=$VERSION

COPY . /app
WORKDIR /app
RUN pip install gunicorn
RUN pip install .

CMD gunicorn -b 0.0.0.0:8080 ui.app:app
